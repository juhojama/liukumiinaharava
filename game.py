import pygame
import random
from pygame.locals import *
from datetime import datetime, date
import sys


class App:
    def __init__(self, width, height, mine_amount):
        self.state = {
            "field": [],
            "opened": []
        }
        self.running = True
        self.display_surface = None
        self.available = []
        self.rects = []

        self.white = (255, 255, 255)
        self.black = (0, 0, 0)

        self.block_size = 40
        self.board_width = width
        self.board_height = height
        self.mine_amount = mine_amount
        self.tiles_opened = 0

        self.size = self.board_width * self.block_size, self.board_height * self.block_size

        self.start_time = datetime.now()
        self.turns_taken = 0
        self.result = None

    def place_mines(self, available_tiles: list, mine_amount: int):
        """"
        Sets the tiles of the board
        """
        random.shuffle(available_tiles)
        for _ in range(mine_amount):
            coordinates = available_tiles.pop()
            self.state["field"][coordinates[1]][coordinates[0]] = "mine"

    def draw_board(self) -> None:
        """"
        Draws the visual board. Used for example when game window is resized.
        """
        if not self.state["field"]:
            for row in range(self.board_height):
                self.state["field"].append([])
                self.state["opened"].append([])
                for col in range(self.board_width):
                    self.state["field"][-1].append("empty")
                    self.state["opened"][-1].append(False)

            for x in range(self.board_width):
                for y in range(self.board_height):
                    self.available.append((x, y))

            self.place_mines(self.available[:], self.mine_amount)

            for row in range(self.board_height):
                for col in range(self.board_width):
                    if "mine" != self.state["field"][row][col]:
                        mine_count = self.calculate_mines_next_to_tile(col, row)
                        if mine_count > 0:
                            self.state["field"][row][col] = str(mine_count)

        for index_y, row in enumerate(self.state["field"]):
            for index_x, tile in enumerate(row):
                if self.state["opened"][index_y][index_x]:
                    sprite = pygame.image.load('sprites/tile_' + tile + '.png').convert()
                else:
                    sprite = pygame.image.load('sprites/tile_back.png').convert()
                rect = sprite.get_rect()
                rect_pos = (index_x * self.block_size, index_y * self.block_size)
                rect.x, rect.y = rect_pos
                self.display_surface.blit(sprite, rect)
                pygame.draw.rect(self.display_surface, self.black, rect, -1)
                self.rects.append(rect)

    def calculate_mines_next_to_tile(self, tile_x: int, tile_y: int) -> int:
        """"
        Calculates mines next to tile
        """
        mine_count = 0

        for i in range(-1, 2, 1):
            for j in range(-1, 2, 1):
                try:
                    if 0 <= tile_x + i <= len(self.state["field"][0]) - 1 and 0 <= tile_y + j <= len(
                            self.state["field"]) - 1:
                        if self.state["field"][tile_y + j][tile_x + i] == 'mine':
                            mine_count += 1
                except IndexError:
                    print('Error when {} {}'.format(str(tile_x), str(tile_y)))
        return mine_count

    def explode(self, tile_x: int, tile_y: int) -> None:
        """"
        Checks for empty tiles and boundaries and reveals them
        """
        if self.state["field"][tile_y][tile_x] != 'mine':
            the_list = [(tile_x, tile_y)]
            loop_counter = 0
            while the_list:
                coords = the_list.pop()
                _x = coords[0]
                _y = coords[1]
                try:
                    if _x > -1 and _y > -1:
                        if not self.state["opened"][_y][_x]:
                            self.reveal_tile(_x, _y)
                            if self.state["field"][_y][_x] == 'empty':
                                for i in range(-1, 2, 1):
                                    for j in range(-1, 2, 1):
                                        the_list.append((_x + j, _y + i))
                except IndexError:
                    pass
                loop_counter += 1

    def reveal_tile(self, tile_x: int, tile_y: int) -> None:
        """"
        Shows tile contents
        """
        sprite = pygame.image.load('sprites/tile_' + self.state["field"][tile_y][tile_x] + '.png').convert()
        sprite = pygame.transform.scale(sprite, (int(self.block_size), int(self.block_size)))
        new_rect = sprite.get_rect()
        new_rect_pos = (tile_x * self.block_size, tile_y * self.block_size)
        new_rect.x, new_rect.y = new_rect_pos
        self.display_surface.blit(sprite, new_rect)
        self.state["opened"][tile_y][tile_x] = True
        self.tiles_opened += 1

    def check_if_victory(self) -> None:
        """"
        Checks if all non-mine tiles are opened
        """
        if self.tiles_opened == (self.board_height * self.board_width) - self.mine_amount:
            self.handle_victory()
        pass

    def handle_defeat(self) -> None:
        print('You lost!')
        self.result = 'Defeat'
        self.save_record()
        self.running = False

    def handle_victory(self) -> None:
        print('You won!')
        self.result = 'Victory'
        self.save_record()
        self.running = False

    def save_record(self) -> None:
        """"
        Saves game results to file
        """
        formatted_start_time = self.start_time.strftime("%d.%m.%Y %H:%M:%S")
        duration = datetime.now() - self.start_time

        fo = open("records.txt", "a+")
        fo.write("## New game ##\n")
        fo.write("Start time: {}\n".format(formatted_start_time))
        fo.write("Duration: {} minutes\n".format(duration.total_seconds() / 60))
        fo.write("Turns taken: {}\n".format(self.turns_taken))
        fo.write("Result: {} with field size of {}x{} and mine amount {}\n".format(self.result, self.board_height,
                                                                                   self.board_width, self.mine_amount))
        fo.close()

    @staticmethod
    def view_records() -> None:
        fo = open("records.txt", "r")
        file_contents = fo.read()
        print(file_contents)
        fo.close()

    def on_init(self) -> None:
        pygame.init()
        self.display_surface = pygame.display.set_mode(self.size,
                                                       pygame.HWSURFACE | pygame.DOUBLEBUF | pygame.RESIZABLE)
        self.display_surface.fill(self.white)
        self.running = True
        self.draw_board()

    def on_event(self, event) -> None:
        if event.type == pygame.QUIT:
            self.running = False
            pygame.display.quit()
            sys.exit()
        if event.type == pygame.VIDEORESIZE:
            self.draw_board()
        if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
            for rect in self.rects:
                if rect.collidepoint(pygame.mouse.get_pos()):
                    tile_x = int(rect.x / self.block_size)
                    tile_y = int(rect.y / self.block_size)
                    if not self.state["opened"][tile_y][tile_x]:
                        self.turns_taken += 1
                        if "mine" != self.state["field"][tile_y][tile_x]:
                            self.explode(tile_x, tile_y)
                            self.check_if_victory()
                        else:
                            self.reveal_tile(tile_x, tile_y)
                            self.handle_defeat()

    @staticmethod
    def on_render():
        pygame.display.update()

    def on_execute(self):
        self.on_init()
        while self.running:
            for event in pygame.event.get():
                self.on_event(event)
            self.on_render()
        pygame.quit()


if __name__ == '__main__':
    print('Welcome to mine sweeper game')
    while True:
        print('1. New game')
        print('2. View records')
        print('3. Quit')
        selection = str(input('> '))
        if selection == '1':
            _height = 0
            _width = 0
            _mine_amount = -1
            try:
                while _height < 1:
                    _height = int(input('Input field height > '))
                while _width < 1:
                    _width = int(input('Input field width > '))
                while _mine_amount < 0:
                    _mine_amount = int(input('Input mine amount > '))
            except ValueError:
                print('Only integers, please')
                exit()
            print('New game begins!')
            newGame = App(_width, _height, _mine_amount)
            newGame.on_execute()
        elif selection == '2':
            App.view_records()
        elif selection == '3':
            exit()
        else:
            print('Choose a valid input!')
